function toAscii(str) {
	let polishAscii = { 'ą': 'a', 'ć' : 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ś': 's', 'ó': 'o', 'ż': 'z', 'ź': 'z',  'Ą': 'A', 'Ć' : 'C', 'Ę': 'E', 'Ł': 'L', 'Ń': 'N', 'Ś': 'S', 'Ó': 'O', 'Ż': 'Z', 'Ź': 'Z'}
	Object.keys(polishAscii).forEach((v,k) => {
		while(true) {
			let index=str.indexOf(v)
			if (index===-1)
				break
			str=str.replace(v,polishAscii[v])
		}		
	})
	return str
}

function communication(site, post, get, waitCode, fRev) {
	if (waitCode !== '' && waitCode !== undefined)
		waitCode();
	if (post === undefined)
		return;
	var contentFile;
//	if (window.XMLHttpRequest)
		contentFile = new XMLHttpRequest();
//	else
//		contentFile = new ActiveXObject("Microsoft.XMLHTTP");
	contentFile.onreadystatechange = function () {		
		if(contentFile.readyState === 4) {
			
			if(contentFile.status === 200 || contentFile.status === 0) {
				fRev(contentFile.responseText);
				return;
			}
			else {
				fRev(-1);
				return;
			}
		}
		
	}	
	if (get !== undefined && get !== '')
		site += get;
	contentFile.open("POST", site, true);
	contentFile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	contentFile.send(post);
}

function getTime() {
	let t = new Date()
	var tdiv=document.querySelector('#time') 
	//tdiv.innerText=getTime()	
	tdiv.innerText= (t.getHours() < 10 ? '0' : '') + t.getHours() + ':' + (t.getMinutes() < 10 ? '0' : '') + t.getMinutes() + ':' + (t.getSeconds() < 10 ? '0' : '') +  t.getSeconds()
	if (t.getHours()===0 && t.getMinutes()===0&& t.getSeconds()===0) getDate()
	setTimeout(() => {getTime()},1000)
}

function getDate() {
	let t = new Date()
	console.log(t.getDay())
	document.querySelector('#date').innerText = (t.getDate() < 9 ? '0' : '') + (t.getDate()) + '/' + (t.getMonth() < 9 ? '0' : '') + (t.getMonth()+1) + '/' + (t.getYear()+1900)
}

function getLocalization() {
	let options = {
		enableHighAccuracy: true,
		timeout: 5000,
		maximumAge: 0
	}

	/*function success(pos) {
		var crd = pos.coords;
		
		console.log('Your current position is:');
		console.log(`Latitude : ${crd.latitude}`);
		console.log(`Longitude: ${crd.longitude}`);
		console.log(`More or less ${crd.accuracy} meters.`);
	}

	function error(err) {
		console.warn(`ERROR(${err.code}): ${err.message}`);
	}*/
	//http://api.openweathermap.org/data/2.5/find?lat=50.7435838&lon=19.266257&appid=332cb95a913228c98649efced1cb5191 //<- API do pogody!
	navigator.geolocation.getCurrentPosition((pos) => {
		var crd = pos.coords
		communication(`https://nominatim.openstreetmap.org/search?q=${crd.latitude}+${crd.longitude}&format=json&polygon=1&addressdetails=1`, '', '', '', (ret) => {
			let json = new Function('return ' + ret)()[0]
			document.querySelector('#village').innerText=json.address.village || json.address.city
			getWeather(json.address.village || json.address.city,crd.latitude,crd.longitude)
			//aktualizacja pogody co godzinę, można zmienić na dowolny czas
			setInterval((city, lat, lon) => {getWeather(city, lat, lon)}, 1000*60*60, json.address.village || json.address.city,crd.latitude,crd.longitude)
			setTimeout(() => {getLocalization()},1000*60*15)
			console.log(json)
		})
	}, () => {return 'Brak danych'}, options);
}

function getWeather(city, lat, lon) {
	city=toAscii(city)
	//city.forEach((v,k) => { polishAscii.forEach( (l,p) => { if (v===l) city[k]=l }) } )
	communication(`http://api.openweathermap.org/data/2.5/find?lat=${lat}&lon=${lon}&units=metric&lang=pl&appid=332cb95a913228c98649efced1cb5191`,'','','', (ret) => {
		let json = new Function('return ' + ret)()
		console.log(json)
		var style
		var temp
		var desc
		var isCity=false
		json.list.forEach((v,k) => {
			if (k==0) {
				style=`background: url('http://openweathermap.org/img/wn/${v.weather[0].icon}@4x.png') center no-repeat;`
				temp= parseInt(v.main.temp) + ' <span style="vertical-align: super;">o</span>C'
				desc= v.weather[0].description + ', ciśnienie ' + v.main.pressure + ' hPa'
			}
			if (v.name===city && !isCity) {
				//wielkość obrazka to 50x50px; można modyfikować poprzez @wielkoscx (obecnie 4x -> 200x200px)
				document.querySelector('#weatherIcon').style=`background: url('http://openweathermap.org/img/wn/${v.weather[0].icon}@4x.png') center no-repeat;`
				document.querySelector('#temperature').innerHTML= parseInt(v.main.temp) + ' <span style="vertical-align: super;">o</span>C'
				document.querySelector('#description').innerText= v.weather[0].description + ', ciśnienie ' + v.main.pressure + ' hPa'	
				isCity=true
			}			
			//console.log(v,k)
		})
		if (!isCity) {
			document.querySelector('#weatherIcon').style=style
			document.querySelector('#temperature').innerHTML=temp
			document.querySelector('#description').innerText=desc	
		}
	})
}

function showBeltInfo() {
	communication('./sites/news.json','','','', (ret) => {
		
		let json = new Function('return ' + ret)()[0]
		console.log(json)
		var belt=""
		json['news'].forEach((v,k) => {
			belt+=`<span>${v['text']}</span> | `
		})
		document.querySelector('#bottombelt').innerHTML=belt
	})
}