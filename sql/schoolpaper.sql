-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 08 Wrz 2021, 22:39
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `schoolpaper`
--
CREATE DATABASE IF NOT EXISTS `schoolpaper` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `schoolpaper`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors` (
  `id_author` bigint(20) UNSIGNED NOT NULL,
  `shortname` varchar(10) NOT NULL,
  `first` varchar(20) NOT NULL,
  `mid` varchar(20) NOT NULL,
  `last` varchar(40) NOT NULL,
  `id_function` bigint(20) UNSIGNED NOT NULL,
  `password` binary(32) NOT NULL,
  `mailname` varchar(100) NOT NULL,
  `id_domain` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `content_articles`
--

DROP TABLE IF EXISTS `content_articles`;
CREATE TABLE `content_articles` (
  `id_article` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `id_type` bigint(20) UNSIGNED NOT NULL,
  `id_author` bigint(20) UNSIGNED NOT NULL,
  `date_create` datetime NOT NULL,
  `content` text NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `content_links`
--

DROP TABLE IF EXISTS `content_links`;
CREATE TABLE `content_links` (
  `id_link` bigint(20) UNSIGNED NOT NULL,
  `id_author` bigint(20) UNSIGNED NOT NULL,
  `add_date` datetime NOT NULL,
  `link` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `content_materials`
--

DROP TABLE IF EXISTS `content_materials`;
CREATE TABLE `content_materials` (
  `id_material` bigint(20) UNSIGNED NOT NULL,
  `id_author` bigint(20) UNSIGNED NOT NULL,
  `add_date` datetime NOT NULL,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `domains`
--

DROP TABLE IF EXISTS `domains`;
CREATE TABLE `domains` (
  `id_domain` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sets`
--

DROP TABLE IF EXISTS `sets`;
CREATE TABLE `sets` (
  `id_set` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_author` bigint(20) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL,
  `description` varchar(500) NOT NULL,
  `time` int(11) NOT NULL COMMENT 'w sekundach',
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sets_articles`
--

DROP TABLE IF EXISTS `sets_articles`;
CREATE TABLE `sets_articles` (
  `id_set` bigint(20) UNSIGNED NOT NULL,
  `id_arcticle` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sets_links`
--

DROP TABLE IF EXISTS `sets_links`;
CREATE TABLE `sets_links` (
  `id_set` bigint(20) UNSIGNED NOT NULL,
  `id_link` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sets_materials`
--

DROP TABLE IF EXISTS `sets_materials`;
CREATE TABLE `sets_materials` (
  `id_set` bigint(20) UNSIGNED NOT NULL,
  `id_material` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sets_plans`
--

DROP TABLE IF EXISTS `sets_plans`;
CREATE TABLE `sets_plans` (
  `id_plan` bigint(20) UNSIGNED NOT NULL,
  `id_set` bigint(20) UNSIGNED NOT NULL,
  `start` time NOT NULL,
  `duration` int(11) UNSIGNED NOT NULL COMMENT 'w sekundach'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
  `id_type` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `authors`
--
ALTER TABLE `authors`
  ADD UNIQUE KEY `id_author` (`id_author`),
  ADD KEY `id_function` (`id_function`),
  ADD KEY `id_domain` (`id_domain`);

--
-- Indeksy dla tabeli `content_articles`
--
ALTER TABLE `content_articles`
  ADD UNIQUE KEY `id_article` (`id_article`),
  ADD KEY `id_type` (`id_type`),
  ADD KEY `id_author` (`id_author`);

--
-- Indeksy dla tabeli `content_links`
--
ALTER TABLE `content_links`
  ADD UNIQUE KEY `id_link` (`id_link`),
  ADD KEY `id_author` (`id_author`);

--
-- Indeksy dla tabeli `content_materials`
--
ALTER TABLE `content_materials`
  ADD UNIQUE KEY `id_material` (`id_material`),
  ADD KEY `id_author` (`id_author`);

--
-- Indeksy dla tabeli `domains`
--
ALTER TABLE `domains`
  ADD UNIQUE KEY `id_domain` (`id_domain`);

--
-- Indeksy dla tabeli `sets`
--
ALTER TABLE `sets`
  ADD KEY `id_author` (`id_author`);

--
-- Indeksy dla tabeli `sets_articles`
--
ALTER TABLE `sets_articles`
  ADD KEY `id_set` (`id_set`),
  ADD KEY `id_arcticle` (`id_arcticle`);

--
-- Indeksy dla tabeli `sets_links`
--
ALTER TABLE `sets_links`
  ADD KEY `id_set` (`id_set`),
  ADD KEY `id_link` (`id_link`);

--
-- Indeksy dla tabeli `sets_materials`
--
ALTER TABLE `sets_materials`
  ADD KEY `id_set` (`id_set`),
  ADD KEY `id_material` (`id_material`);

--
-- Indeksy dla tabeli `sets_plans`
--
ALTER TABLE `sets_plans`
  ADD UNIQUE KEY `id_plan` (`id_plan`),
  ADD KEY `id_set` (`id_set`);

--
-- Indeksy dla tabeli `types`
--
ALTER TABLE `types`
  ADD UNIQUE KEY `id_type` (`id_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `authors`
--
ALTER TABLE `authors`
  MODIFY `id_author` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `content_articles`
--
ALTER TABLE `content_articles`
  MODIFY `id_article` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `content_links`
--
ALTER TABLE `content_links`
  MODIFY `id_link` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `content_materials`
--
ALTER TABLE `content_materials`
  MODIFY `id_material` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `domains`
--
ALTER TABLE `domains`
  MODIFY `id_domain` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `sets_plans`
--
ALTER TABLE `sets_plans`
  MODIFY `id_plan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `types`
--
ALTER TABLE `types`
  MODIFY `id_type` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
